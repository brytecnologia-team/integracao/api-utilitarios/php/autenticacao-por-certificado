<?php

    $url = 'https://hub2.bry.com.br/api/authentication-service/v1/verify-challenge';
    //https://hub2.hom.bry.com.br/api/authentication-service/v1/verify-challenge
    $caminhoCertificado = './caminho/para/o/certificado/';
    $senhaCertificado = 'senha do certificado';
    $token = 'insert-a-valid-token';

    $mode = 'BASIC';
    $hashAlgorithm = 'SHA1';

    //Gera uma String aleatória em base64 para ser assinada (Desafio)
    function gerarDesafio()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $desafio = '';
        for ($i = 0; $i < 64; $i++) {
            $desafio .= $characters[rand(0, $charactersLength - 1)];
        }

        return base64_encode($desafio);
    }

    //Assina com certificado armazenado em disco e retorna o valor da assinatura como uma string base64
    function criptografaConteudo($conteudo)
    {
        global $hashAlgorithm;

        echo "================  Inicializando assinatura  ================\n\n";

        // COLETA A CHAVE PRIVADA DO CERTIFICADO
        list(,$privkey) = getCertificadoEChavePrivada();

        // DECODIFICA O BASE64 QUE ESTÁ NA VARIÁVEL "conteudo"
        $conteudoDecodificado = base64_decode($conteudo);


        // CIFRA/ASSINA O CONTEÚDO COM A CHAVE PRIVADA DO CERTIFICADO
        openssl_sign($conteudoDecodificado, $crypted, $privkey, $hashAlgorithm);

        // CODIFICA EM BASE64 O CONTEÚDO RETORNADO DA CIFRAGEM DOS DADOS
        $cifrado = base64_encode($crypted);
        echo "Valor da assinatura em Base64 ";
        echo $cifrado;
        echo "\n\n_________________________________________________\n\n";

        return $cifrado;
    }

    //Calcula o PEM do certificado (para enviar junto a requisição) e a chave privada (para realizar a assinatura)
    function getCertificadoEChavePrivada()
    {
        global $caminhoCertificado, $senhaCertificado;

        // EXTRAI A CHAVE PRIVADA E O CONTEÚDO DO CERTIFICADO.
        $cert_store = file_get_contents($caminhoCertificado);
        openssl_pkcs12_read($cert_store, $certs, $senhaCertificado);
        $privkey = $certs['pkey'];
        $certificado = $certs['cert'];

        // RETIRA DA STRING DO CERTIFICADO PARTES NÃO DESEJADAS
        $certificadoTratado = str_replace('-----BEGIN CERTIFICATE-----', '', $certificado);
        $certificadoTratado = str_replace('-----END CERTIFICATE-----', '', $certificadoTratado);
        $certificadoTratado = str_replace("\n", '', $certificadoTratado);
        return array($certificadoTratado, $privkey);
    }

    //Verifica desafio
    function verificarDesafio($certificadoTratado, $desafio, $cifrado)
    {
        global $hashAlgorithm, $mode, $token, $url;

        $curlDesafio = curl_init();

        //Json com os dados da requisição
        $json = '{  "certificate" : "' . $certificadoTratado . '",
                    "mode" : "' . $mode . '",
                    "hashAlgorithm" : "' . $hashAlgorithm . '",
                    "challenge" : "' . $desafio . '",
                    "challengeSignature" : "' . $cifrado . '"
                }';

        echo "============= Inicializando Verificação de Desafio no BRy HUB =============\n\n";
        echo "Desafio: " . $desafio . "\n";

        curl_setopt_array($curlDesafio, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "Authorization:" . $token
            ),
        ));

        // ENVIA A REQUISIÇÃO
        $respostaDesafio = curl_exec($curlDesafio);
        $httpcode = curl_getinfo($curlDesafio, CURLINFO_HTTP_CODE);
        curl_close($curlDesafio);

        $respostaDesafioJson = json_decode($respostaDesafio);

        if ($httpcode == 200) {
            echo "\nDesafio verificado com sucesso.\n";
            $statusCertificado = $respostaDesafioJson->certificateReport->status->status;
            $statusAssinatura = $respostaDesafioJson->signatureVerified;

            echo "O certificado do assinante está com status: " . $statusCertificado . "\n";
            echo "A assinatura do desafio esta com valor: " . $statusAssinatura . "\n\n";

            if ($statusCertificado == 'VALID' and  $statusAssinatura == 'true') {
                return true;
            }
        } else {
            echo $respostaDesafio;
            echo "\n";
        }

        return false;
    }

    //Passo 1:
    //Gera o desafio que será assinado pelo certificado. Este passo deve ser realizado pelo servidor de autenticação.
    $desafio = gerarDesafio();

    //Passo 2:
    //Carrega a chave privada e o conteudo do certificado digital
    list($certificadoTratado) = getCertificadoEChavePrivada();

    //Passo 3:
    //Assina o desafio utilizando o certificado em disco
    $cifrado = criptografaConteudo($desafio);

    //Passo 4:
    //Realiza a requisição para o HUB para validação da assinatura e certificado, verificando se o desafio foi aceito. Este passo deve ser realizado pelo servidor de autenticação.
    if (verificarDesafio($certificadoTratado, $desafio, $cifrado)) {
        echo "Usuário autenticado com sucesso!\n";
    } else {
        echo "Usuário não pode ser autenticado.\n";
    }
?>