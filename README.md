# Verificação de Desafio para Autenticação

Este é um exemplo de integração dos serviços da API de validação de certificados para realizar a prova de posse de um certificado digital com clientes baseados em tecnologia PHP. 

Este exemplo apresenta os passos necessários para realizar a prova de posse de um certificado digital:
  - Passo 1 (Servidor): Geração de um desafio que será utilizado durante a prova de posse.
  - Passo 2 (Cliente): Carregamento da chave privada e conteúdo do certificado digital.
  - Passo 3 (Cliente): Assinatura do desafio utilizando a chave privada do certificado.
  - Passo 4 (Servidor): Verificação da assinatura do desafio e do certificado digital utilizado para realizar a assinatura.

**Observação**

A validação da assinatura do desafio e do certificado é realizada de maneira independente, portanto para provar a posse da chave privada correspondente àquele certificado é necessário verificar tanto o status da assinatura quanto o status do certificado.

### Tech

O exemplo utiliza das bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development.
* [CURL] - Client URL Library.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a localização deste arquivo no computador, bem como a senha para acessar seu conteúdo.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para cifragem do desafio e verificação de posse.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| $token | Access Token para o consumo do serviço (JWT). | challenge.php
| $caminhoCertificado | Localização do arquivo da chave privada a ser configurada na aplicação. | challenge.php
| $senhaCertificado | Senha do arquivo da chave privada a ser configurada na aplicação. | challenge.php
| $mode | Modo de retorno da validação do certificado. | challenge.php
| $hashAlgorithm | Algoritmo de hash utilizado na assinatura do desafio. | challenge.php

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).

### Uso

Para execução da aplicação de exemplo é necessário ter o PHP 7.x instalado na sua máquina, além de instalar a biblioteca php-curl.

Comandos:

Instalar php-curl (linux):

    -apt-get install php-curl

Caso o seu sistema operacional seja outro, verifique sobre a instalação [aqui](https://www.php.net/manual/pt_BR/curl.requirements.php).

Executar programa:

    -php challenge.php

   [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
   [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>